* Re-implementation of ChrootManagerFragment.
* Enhancement of permission check.
* Enhancement of sharepreference check.
* Re-defining NhPaths variables.
* Enhancement of nethunter scripts for chroot management.
* Added XZ standalone executable for both arm64 or armhf.
